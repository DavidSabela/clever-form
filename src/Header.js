import React from "react";
import logo from "./logo.svg";
import { Stack } from "@fluentui/react/lib/Stack";
import { itemStyles, wrapStackTokens, stackStyles } from "./Styles";

const Header = () => {
  return (
    <Stack horizontal wrap styles={stackStyles} tokens={wrapStackTokens}>
      <Stack.Item grow style={itemStyles}>
        <img src={logo} alt="logo" />
      </Stack.Item>
      <Stack.Item grow style={itemStyles}>
        {" "}
        <h1>CLEVER-FORM</h1>
      </Stack.Item>
    </Stack>
  );
};

export default Header;
