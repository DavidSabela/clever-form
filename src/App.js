import "./App.css";
import React from "react";
import Header from "./Header";
import FormControler from "./components/FormControler";
import "react-notifications/lib/notifications.css";
import { NotificationContainer } from "react-notifications";
import { Stack } from "@fluentui/react/lib/Stack";

function App() {
  return (
    <Stack>
      <Header />
      <FormControler />
      <NotificationContainer />
    </Stack>
  );
}

export default App;
