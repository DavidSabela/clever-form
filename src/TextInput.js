import { TextField } from "@fluentui/react/lib/TextField";

export const InputField = ({ field, form: { touched, errors }, ...props }) => {
  const errorMsg = touched[field.name] && errors[field.name];
  return (
    <TextField
      {...props}
      {...field}
      errorMessage={errorMsg}
      defaultValue={undefined}
    />
  );
};
