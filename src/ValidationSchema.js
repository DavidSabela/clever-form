import * as yup from "yup";

export const Schema = yup.object().shape({
  name: yup.string().required("This is required field"),
  surname: yup.string().required("This is required field"),
  email: yup
    .string()
    .required("This is required field")
    .email("This is not a valid email address"),
  date: yup.date(),
  region: yup.string(),
  react: yup.bool()
});
