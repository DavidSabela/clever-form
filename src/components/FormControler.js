import React, { useState, useRef } from "react";
import Form from "./Form";
import { TextField } from "@fluentui/react/lib/TextField";
import { NotificationManager } from "react-notifications";
import { Schema } from "../ValidationSchema";
import { Stack } from "@fluentui/react/lib/Stack";
import { itemStyles, wrapStackTokens, stackStyles } from "../Styles";

const FormControler = () => {
  const [formData, setFormData] = useState("{}");
  const [load, setLoad] = useState(false);
  const [loadData, setLoadData] = useState(null);

  const areaField = useRef();

  const handleSubmit = data => {
    setLoadData(JSON.stringify(data));
    setLoad(false);
  };

  const handleLoadData = () => {
    let data = loadData;

    Schema.validate(JSON.parse(data))
      .then(data => {
        setLoad(true);
        setFormData(data || "{}");
        setLoadData(null);
      })
      .catch(err => {
        NotificationManager.error(err.errors, err.name, 2000);
      });
  };

  const updateData = e => {
    setLoadData(e.target.value);
  };

  return (
    <>
      <Form
        formProps={load ? formData : null}
        submit={handleSubmit}
        loadData={handleLoadData}
      />
      <Stack reversed wrap styles={stackStyles} tokens={wrapStackTokens}>
        <Stack.Item align="stretch" style={itemStyles}>
          <TextField
            ref={areaField}
            label="JSON"
            multiline
            rows={3}
            value={loadData}
            onChange={updateData}
          />
        </Stack.Item>
      </Stack>
    </>
  );
};

export default FormControler;
