import React from "react";
import { InputField } from "../TextInput";
import { Stack } from "@fluentui/react/lib/Stack";
import { DefaultButton } from "@fluentui/react/lib/Button";
import { Formik, Field } from "formik";
import "react-notifications/lib/notifications.css";
import { NotificationManager } from "react-notifications";
import { Dropdown } from "@fluentui/react/lib/Dropdown";
import { DatePicker } from "@fluentui/react/lib/DatePicker";
import { Checkbox } from "@fluentui/react/lib/Checkbox";
import { Schema } from "../ValidationSchema";
import { initializeIcons } from "@fluentui/react/lib/Icons";
import { itemStyles, wrapStackTokens, stackStyles } from "../Styles";

initializeIcons(undefined, { disableWarnings: true });

export default function Form({ formProps, submit, loadData }) {
  const init = () => {
    if (formProps) {
      return { ...formProps, ...{ date: new Date(formProps.date) } };
    }
    return {
      name: "",
      surname: "",
      email: "",
      region: "",
      date: "",
      react: false
    };
  };

  return (
    <div>
      <Formik
        enableReinitialize={true}
        initialValues={init()}
        validationSchema={Schema}
        onSubmit={(
          values,
          { setSubmitting, setErrors, setStatus, resetForm }
        ) => {
          try {
            submit(values);
            NotificationManager.success(
              "Data has been sended!",
              "Successful!",
              2000
            );
            resetForm();
          } catch (error) {
            setStatus({ success: false });
            setSubmitting(false);
            NotificationManager.error(error.message, "ERROR!", 2000);
            setErrors({ submit: error.message });
          }
        }}
      >
        {({ values, setFieldValue, handleSubmit }) => (
          <form onSubmit={handleSubmit}>
            <Stack
              horizontal
              wrap
              styles={stackStyles}
              tokens={wrapStackTokens}
            >
              <Stack.Item grow style={itemStyles}>
                {" "}
                <Field
                  label="Jméno"
                  required
                  name="name"
                  value={values.name || ""}
                  placeholder="..."
                  component={InputField}
                />
              </Stack.Item>
              <Stack.Item grow style={itemStyles}>
                <Field
                  label="Příjmení"
                  required
                  name="surname"
                  value={values.surname || ""}
                  placeholder="..."
                  component={InputField}
                />
              </Stack.Item>
            </Stack>
            <Stack
              horizontal
              wrap
              styles={stackStyles}
              tokens={wrapStackTokens}
            >
              <Stack.Item grow style={itemStyles}>
                <Field
                  label="E-mail"
                  required
                  name="email"
                  value={values.email || ""}
                  placeholder="..."
                  component={InputField}
                />
              </Stack.Item>
              <Stack.Item grow style={itemStyles}>
                <Field
                  label="Datum"
                  name="date"
                  required
                  value={values.date}
                  onSelectDate={e => setFieldValue("date", e)}
                  placeholder="Zvolte datum"
                  component={DatePicker}
                />
              </Stack.Item>
            </Stack>
            <Stack reversed wrap styles={stackStyles} tokens={wrapStackTokens}>
              <Stack.Item align="stretch" style={itemStyles}>
                <Field
                  label="REACT"
                  name="react"
                  checked={values.react}
                  onChange={e => setFieldValue("react", e.target.checked)}
                  component={Checkbox}
                />
              </Stack.Item>
              <Stack.Item align="stretch" style={itemStyles}>
                <Field
                  label="Kraj"
                  required
                  name="region"
                  selectedKey={values.region}
                  options={[
                    { key: "Liberecký", text: "Liberecký" },
                    { key: "Moravskoslezský ", text: "Moravskoslezský " }
                  ]}
                  option
                  onChange={(_e, option) => {
                    setFieldValue("region", option.key);
                  }}
                  placeholder="Zvolte z možností"
                  component={Dropdown}
                />
              </Stack.Item>
            </Stack>

            <Stack
              horizontal
              wrap
              styles={stackStyles}
              tokens={wrapStackTokens}
            >
              <Stack.Item grow style={itemStyles}>
                <DefaultButton type="submit" text="Uložit" />
              </Stack.Item>{" "}
              <Stack.Item grow style={itemStyles}>
                <DefaultButton text="Načíst" onClick={() => loadData()} />
              </Stack.Item>
            </Stack>
          </form>
        )}
      </Formik>
    </div>
  );
}
