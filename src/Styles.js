export const itemStyles = {
  root: {
    alignItems: "center",
    display: "flex",
    height: 50,
    justifyContent: "center"
  }
};

export const wrapStackTokens = {
  childrenGap: 20,
  padding: 10
};

export const stackStyles = {
  root: {
    width: 600
  }
};
